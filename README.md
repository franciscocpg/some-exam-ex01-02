# some-exam-ex01-02
[![wercker status](https://app.wercker.com/status/79b5428a136603c3f8fe764b90319ed3/m/master "wercker status")](https://app.wercker.com/project/bykey/79b5428a136603c3f8fe764b90319ed3)

Foi utilizado nos exercícios propostos 1 e 2 `spring boot` por se tratar de um framework (ou micro framework) de aplicação web embarcado e *compliance* com [12 factor](http://12factor.net/).

Como banco de dados, foi utilizado [h2](http://www.h2database.com/html/main.html) com persistência em arquivo durante a execução das aplicações e com persistência em memória durante os testes.

Como os exercícios 1 e 2 possuem dependências entre si e código comum aos dois, foram criados sub-módulos do `maven` para separar as aplicações durante a execução, mas manter as duas juntas como projeto, além de um terceiro sub-módulo para compartilhar as classes comuns.

Como ferramenta de CI foi utilizado [wercker](http://www.wercker.com).

### Testando
Para testar a aplicação basta executar o comando abaixo na raiz do projeto.
```
mvn clean test
```
### Executando
Para executar a aplicação devemos entrar na pasta do ex 01 e subir a aplicação e, em seguida, fazer a mesma coisa na aplicação ex 02.
Sendo assim devemos abrir um terminal na raiz do projeto e digitar:
```
cd some-exam-ex01
mvn spring-boot:run
```
Em seguida abrir outro terminal também na raiz do projeto e digitar:
```
cd some-exam-ex02
mvn spring-boot:run
```
Após as aplicações subirem, podemos acessar a aplicação **some-exam-ex01** no endereço http://localhost:8080 e a aplicação **some-exam-ex02** no endereço http://localhost:8081.

Toda a documentação necessária para a utilização da API está disponível nessas URLs através do [swagger](http://swagger.io/) sendo possível utilizar os endpoints de forma simples, mas também gerando exemplo de chamadas com `curl` caso o desenvolvedor deseje utilizar o terminal.

Nas classes de testes estão documentados os critérios de aceites propostos, seja através de javadoc ou pelo nome do método de teste.