package com.francisco.guimaraes.some.exam.ex01.controller;

import com.francisco.guimaraes.shared.model.CEPRequest;
import com.francisco.guimaraes.some.exam.ex01.Application;
import static com.francisco.guimaraes.some.exam.ex01.util.TestUtils.post;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import org.springframework.web.context.WebApplicationContext;

/**
 * CEP service test
 *
 * @author Francisco Guimarães
 * @since 25/06/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class CEPControllerTests {

    @Autowired
    private WebApplicationContext ctx;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(ctx).build();
    }

    /**
     * Critério de aceite. <br>
     * - Dado um CEP válido, deve retornar o endereço correspondente.
     */
    @Test
    public void whenIPostValidCEP_ShouldReturn200() throws Exception {
        this.mockMvc.perform(post("/cep", CEPRequest.create("03113010")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rua").value("av dos testes"))
                .andExpect(jsonPath("$.bairro").value("Testes"))
                .andExpect(jsonPath("$.cidade").value("São Paulo"))
                .andExpect(jsonPath("$.estado").value("SP"));
    }

    /**
     * Critério de aceite. <br>
     * - Dado um CEP válido, que não exista o endereço, deve substituir um
     * digito da direita para a esquerda por zero até que seja localizado.
     */
    @Test
    public void whenIPostValidCEPAndMatchZeroFromRightToLeft_ShouldReturn200() throws Exception {
        this.mockMvc.perform(post("/cep", CEPRequest.create("03113011")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rua").value("av dos testes"))
                .andExpect(jsonPath("$.bairro").value("Testes"))
                .andExpect(jsonPath("$.cidade").value("São Paulo"))
                .andExpect(jsonPath("$.estado").value("SP"));
        this.mockMvc.perform(post("/cep", CEPRequest.create("03113031")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rua").value("alameda dos testes"))
                .andExpect(jsonPath("$.bairro").value("Testes"))
                .andExpect(jsonPath("$.cidade").value("São Paulo"))
                .andExpect(jsonPath("$.estado").value("SP"));
    }

    /**
     * Não há critério de aceite explícito para o cenário abaixo, mas caso o CEP
     * seja válido e não encontrado, devemos retornar 204.
     */
    @Test
    public void whenIPostNotFoundedCEP_ShouldReturn404() throws Exception {
        this.mockMvc.perform(post("/cep", CEPRequest.create("03112011")))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * Critério de aceite. <br>
     * - Dado um CEP inválido, deve retornar uma mensagem reportando o erro:
     * "CEP inválido".
     */
    @Test
    public void whenIPostInvalidCEP_ShouldReturn400() throws Exception {
        this.mockMvc.perform(post("/cep", CEPRequest.create("0311301")))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("CEP inválido"));
    }
}
