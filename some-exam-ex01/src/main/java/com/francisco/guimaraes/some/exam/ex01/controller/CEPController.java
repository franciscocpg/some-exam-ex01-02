package com.francisco.guimaraes.some.exam.ex01.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.francisco.guimaraes.shared.model.CEPRequest;
import com.francisco.guimaraes.shared.model.Endereco;
import com.francisco.guimaraes.shared.exception.RestException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Francisco Guimarães
 * @since 25/06/2016
 */
@RestController
public class CEPController {

    private static final Map<String, Endereco> MOCKED_CEP = new HashMap<>();
    private final Pattern cepPattern = Pattern.compile("\\d{8}");

    static {
        initMock();
    }

    private static void initMock() {
        MOCKED_CEP.put("01331000",
                new Endereco.EnderecoBuilder()
                .setRua("rua do teste")
                .setBairro("Teste")
                .setCidade("São Paulo")
                .setEstado("SP")
                .createEndereco());
        MOCKED_CEP.put("03113010",
                new Endereco.EnderecoBuilder()
                .setRua("av dos testes")
                .setBairro("Testes")
                .setCidade("São Paulo")
                .setEstado("SP")
                .createEndereco());
        MOCKED_CEP.put("03113000",
                new Endereco.EnderecoBuilder()
                .setRua("alameda dos testes")
                .setBairro("Testes")
                .setCidade("São Paulo")
                .setEstado("SP")
                .createEndereco());
    }

    @RequestMapping(path = "/cep",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE},
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @JsonView(Endereco.CEPResponse.class)
    public ResponseEntity<Endereco> findCEP(@RequestBody CEPRequest request)
            throws Exception {
        String cep = request.getCEP();
        if (!cepPattern.matcher(cep).matches()) {
            throw new RestException(HttpStatus.BAD_REQUEST, "CEP inválido");
        }
        if ((cep = findCep(cep)) == null) {
            throw new RestException(HttpStatus.NOT_FOUND, "CEP não encontrado");
        }
        return new ResponseEntity<>(MOCKED_CEP.get(cep), HttpStatus.OK);
    }

    private String findCep(String cep) {
        StringBuilder cepBuilder = new StringBuilder(cep);
        int pos = cep.length();
        while (--pos >= 0) {
            if (MOCKED_CEP.containsKey(cep)) {
                return cep;
            }
            cep = cepBuilder.replace(pos, pos + 1, "0").toString();
        }
        return null;
    }
}
