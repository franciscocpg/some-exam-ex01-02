package com.francisco.guimaraes.some.exam.ex02.config;

import com.francisco.guimaraes.shared.exception.RestException;
import com.francisco.guimaraes.shared.model.CEPRequest;
import com.francisco.guimaraes.shared.model.Endereco;
import com.francisco.guimaraes.some.exam.ex02.client.CepClient;
import java.io.IOException;
import static org.junit.Assert.assertNotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Francisco Guimarães
 * @since 29/06/2016
 */
@Profile("test")
@Configuration
public class FeignConfigTest {

    public static HttpStatus httpStatus;

    @Bean
    @Primary
    CepClient configCepClient() throws IOException {
        return (CEPRequest request) -> {
            assertNotNull("HttpStatus must not be null", httpStatus);
            if (!httpStatus.equals(HttpStatus.OK)) {
                throw new RestException(httpStatus, request.getCEP());
            }
            return new Endereco();
        };
    }

}
