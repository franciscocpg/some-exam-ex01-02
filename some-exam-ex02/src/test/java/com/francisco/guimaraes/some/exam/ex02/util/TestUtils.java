package com.francisco.guimaraes.some.exam.ex02.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 *
 * @author Francisco Guimarães
 * @since 26/06/2016
 */
public class TestUtils {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private TestUtils() {
    }

    public static MockHttpServletRequestBuilder get(String url, Object id)
            throws JsonProcessingException {
        return MockMvcRequestBuilders.get(url, id);
    }

    public static MockHttpServletRequestBuilder post(String url, Object content)
            throws JsonProcessingException {
        return MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(MAPPER.writeValueAsString(content));
    }

    public static MockHttpServletRequestBuilder put(String url, Object id, Object content)
            throws JsonProcessingException {
        return MockMvcRequestBuilders.put(url, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(MAPPER.writeValueAsString(content));
    }

    public static MockHttpServletRequestBuilder delete(String url, Object id)
            throws JsonProcessingException {
        return MockMvcRequestBuilders.delete(url, id);
    }

}
