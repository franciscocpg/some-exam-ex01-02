/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.some.exam.ex02.repository;

import com.francisco.guimaraes.shared.model.Endereco;
import com.francisco.guimaraes.some.exam.ex02.Application;
import com.francisco.guimaraes.some.exam.ex02.config.FeignConfigTest;
import static com.francisco.guimaraes.some.exam.ex02.util.TestUtils.delete;
import static com.francisco.guimaraes.some.exam.ex02.util.TestUtils.get;
import static com.francisco.guimaraes.some.exam.ex02.util.TestUtils.post;
import static com.francisco.guimaraes.some.exam.ex02.util.TestUtils.put;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 *
 * @author Francisco Guimarães
 * @since 25/06/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("test")
public class EnderecoControllerTest {

    private static final String BAIRRO = "bairro";
    private static final String CEP = "01331010";
    private static final String RUA = "rua";
    private static final String NUMERO = "1";
    private static final String ESTADO = "estado";
    private static final String COMPLEMENTO = "complemento";
    private static final String CIDADE = "cidade";

    @Autowired
    private WebApplicationContext ctx;
    @Autowired
    private EnderecoRepository enderecoRepository;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(ctx).build();
        enderecoRepository.deleteAll();
    }

    @Test
    public void whenICreateEnderecoSucessfully_shouldReturn201() throws Exception {
        FeignConfigTest.httpStatus = HttpStatus.OK;

        this.mockMvc.perform(post("/endereco", buildEndereco().createEndereco()))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.bairro").value(BAIRRO))
                .andExpect(jsonPath("$.cep").value(CEP))
                .andExpect(jsonPath("$.cidade").value(CIDADE))
                .andExpect(jsonPath("$.complemento").value(COMPLEMENTO))
                .andExpect(jsonPath("$.estado").value(ESTADO))
                .andExpect(jsonPath("$.numero").value(NUMERO))
                .andExpect(jsonPath("$.rua").value(RUA));
    }

    @Test
    public void whenICreateEnderecoWithInvalidCEP_shouldReturn400() throws Exception {
        FeignConfigTest.httpStatus = HttpStatus.BAD_REQUEST;

        this.mockMvc.perform(post("/endereco", buildEndereco().createEndereco()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenICreateEnderecoWithNotFoundedCEP_shouldReturn404() throws Exception {
        FeignConfigTest.httpStatus = HttpStatus.NOT_FOUND;

        this.mockMvc.perform(post("/endereco", buildEndereco().createEndereco()))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenIUpdateEnderecoSucessfully_shouldReturn200() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());
        String newBairro = BAIRRO + "1";
        endereco.setBairro(newBairro);
        FeignConfigTest.httpStatus = HttpStatus.OK;

        this.mockMvc.perform(put("/endereco/{id}", endereco.getId(), endereco))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bairro").value(newBairro))
                .andExpect(jsonPath("$.cep").value(CEP))
                .andExpect(jsonPath("$.cidade").value(CIDADE))
                .andExpect(jsonPath("$.complemento").value(COMPLEMENTO))
                .andExpect(jsonPath("$.estado").value(ESTADO))
                .andExpect(jsonPath("$.numero").value(NUMERO))
                .andExpect(jsonPath("$.rua").value(RUA));
    }

    @Test
    public void whenIUpdateEnderecoNotFoundedSucessfully_shouldReturn404() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());
        String newBairro = BAIRRO + "1";
        endereco.setBairro(newBairro);
        FeignConfigTest.httpStatus = HttpStatus.OK;

        this.mockMvc.perform(put("/endereco/{id}", endereco.getId() + 1, endereco))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenIUpdateEnderecoWithInvalidCEP_shouldReturn400() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());
        String newBairro = BAIRRO + "1";
        endereco.setBairro(newBairro);
        FeignConfigTest.httpStatus = HttpStatus.BAD_REQUEST;

        this.mockMvc.perform(put("/endereco/{id}", endereco.getId(), endereco))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenIUpdateEnderecoWithNotFoundedCEP_shouldReturn404() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());
        String newBairro = BAIRRO + "1";
        endereco.setBairro(newBairro);
        FeignConfigTest.httpStatus = HttpStatus.NOT_FOUND;

        this.mockMvc.perform(put("/endereco/{id}", endereco.getId(), endereco))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenIDeleteEnderecoSucessfully_shouldReturn204() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());

        this.mockMvc.perform(delete("/endereco/{id}", endereco.getId()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void whenIDeleteEnderecoNotFounded_shouldReturn404() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());

        this.mockMvc.perform(delete("/endereco/{id}", endereco.getId() + 1))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenIGetEnderecoSucessfully_shouldReturn200() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());

        this.mockMvc.perform(get("/endereco/{id}", endereco.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bairro").value(BAIRRO))
                .andExpect(jsonPath("$.cep").value(CEP))
                .andExpect(jsonPath("$.cidade").value(CIDADE))
                .andExpect(jsonPath("$.complemento").value(COMPLEMENTO))
                .andExpect(jsonPath("$.estado").value(ESTADO))
                .andExpect(jsonPath("$.numero").value(NUMERO))
                .andExpect(jsonPath("$.rua").value(RUA));
    }

    @Test
    public void whenIGetEnderecoNotFounded_shouldReturn404() throws Exception {
        Endereco endereco = enderecoRepository.save(buildEndereco().createEndereco());
        String newBairro = BAIRRO + "1";
        endereco.setBairro(newBairro);

        this.mockMvc.perform(get("/endereco/{id}", endereco.getId() + 1))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    private static Endereco.EnderecoBuilder buildEndereco() {
        return new Endereco.EnderecoBuilder()
                .setBairro(BAIRRO)
                .setCep(CEP)
                .setCidade(CIDADE)
                .setComplemento(COMPLEMENTO)
                .setEstado(ESTADO)
                .setNumero(NUMERO)
                .setRua(RUA);
    }
}
