/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.some.exam.ex02.repository;

import com.francisco.guimaraes.shared.exception.RestException;
import com.francisco.guimaraes.shared.model.CEPRequest;
import com.francisco.guimaraes.shared.model.Endereco;
import com.francisco.guimaraes.some.exam.ex02.client.CepClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 *
 * @author Francisco Guimarães
 * @since 29/06/2016
 */
@RepositoryRestController
public class EnderecoController {

    private final EnderecoRepository enderecoRepository;

    private final CepClient cepClient;

    @Autowired
    public EnderecoController(EnderecoRepository enderecoRepository, CepClient cepClient) {
        this.enderecoRepository = enderecoRepository;
        this.cepClient = cepClient;
    }

    @RequestMapping(method = POST, value = "/endereco")
    public ResponseEntity<Endereco> create(@RequestBody Endereco endereco) throws Exception {
        validateCep(endereco);
        return ResponseEntity.status(HttpStatus.CREATED).body(enderecoRepository.save(endereco));
    }

    @RequestMapping(method = PUT, value = "/endereco/{id}")
    public ResponseEntity<Endereco> update(@RequestBody Endereco endereco,
            @PathVariable("id") Long id) throws Exception {
        Endereco updateEndereco = enderecoRepository.findOne(id);
        if (updateEndereco == null) {
            throw new RestException(HttpStatus.NOT_FOUND, "");
        }
        endereco.setId(updateEndereco.getId());
        validateCep(endereco);
        return ResponseEntity.ok(enderecoRepository.save(endereco));
    }

    private void validateCep(Endereco endereco) throws Exception {
        cepClient.findCep(CEPRequest.create(endereco.getCep()));
    }

}
