package com.francisco.guimaraes.some.exam.ex02.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.francisco.guimaraes.shared.exception.RestException;
import com.francisco.guimaraes.shared.model.ResponseError;
import com.francisco.guimaraes.some.exam.ex02.client.CepClient;
import feign.Feign;
import feign.Response;
import feign.Util;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author Francisco Guimarães
 * @since 29/06/2016
 */
@Configuration
@Profile("!test")
public class FeignConfig {

    @Value("${service.cep.host}")
    private String serviceCepHost;

    @Bean
    CepClient configCepClient() throws IOException {
        return Feign.builder()
                .errorDecoder((String methodKey, Response response) -> {
                    try {
                        String body = Util.toString(response.body().asReader());
                        ResponseError responseError
                                = new ObjectMapper()
                                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                                .readValue(body, ResponseError.class);
                        return new RestException(
                                responseError.getCode(),
                                responseError.getMessage());
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                })
                .encoder(new JacksonEncoder())//
                .decoder(new JacksonDecoder())//
                .target(CepClient.class, serviceCepHost);
    }

}
