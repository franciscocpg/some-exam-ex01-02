package com.francisco.guimaraes.some.exam.ex02.repository;

import com.francisco.guimaraes.shared.model.Endereco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 *
 * @author Francisco Guimarães
 * @since 29/06/2016
 */
@RepositoryRestResource(collectionResourceRel = "endereco", path = "endereco")
public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

    @Override
    @RestResource(exported = false)
    public Endereco save(Endereco endereco);

}
