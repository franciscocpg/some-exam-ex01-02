/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.some.exam.ex02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Francisco Guimarães
 * @since 25/06/2016
 */
@SpringBootApplication
@EntityScan("com.francisco.guimaraes.shared")
@ComponentScan("com.francisco.guimaraes")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
