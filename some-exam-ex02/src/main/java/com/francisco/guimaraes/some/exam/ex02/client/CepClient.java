/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.some.exam.ex02.client;

import com.francisco.guimaraes.shared.exception.RestException;
import com.francisco.guimaraes.shared.model.CEPRequest;
import com.francisco.guimaraes.shared.model.Endereco;
import feign.Headers;
import feign.RequestLine;

/**
 *
 * @author Francisco Guimarães
 * @since 29/06/2016
 */
public interface CepClient {

    @RequestLine("POST /cep")
    @Headers("Content-Type: application/json")
    Endereco findCep(CEPRequest request) throws RestException;
}
