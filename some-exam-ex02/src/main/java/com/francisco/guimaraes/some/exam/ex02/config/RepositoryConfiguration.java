package com.francisco.guimaraes.some.exam.ex02.config;

import com.francisco.guimaraes.shared.model.Endereco;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 *
 * @author Francisco Guimarães
 * @since 30/06/2016
 */
@Configuration
public class RepositoryConfiguration extends RepositoryRestMvcConfiguration {

    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Endereco.class);
    }
}
