package com.francisco.guimaraes.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Francisco Guimarães
 * @since 26/06/2016
 */
public class CEPRequest {

    private String CEP;

    public static CEPRequest create(String CEP) {
        return new CEPRequest(CEP);
    }

    public CEPRequest() {
    }

    private CEPRequest(String CEP) {
        this.CEP = CEP;
    }

    public String getCEP() {
        return CEP;
    }

    @JsonProperty(value = "CEP", required = true)
    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

}
