/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.shared.controller;

import com.francisco.guimaraes.shared.model.ResponseError;
import com.francisco.guimaraes.shared.exception.RestException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author Francisco Guimarães
 * @since 30/06/2016
 */
@ControllerAdvice
class GlobalControllerExceptionHandler {

    @ExceptionHandler(RestException.class)
    public ResponseEntity handle(HttpServletRequest req, RestException ex) {
        return ResponseEntity
                .status(ex.getCode())
                .body(new ResponseError(ex.getCode(), ex.getMessage(), req.getRequestURI()));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handle(HttpServletRequest req, ConstraintViolationException ex) {
        StringBuilder sb = new StringBuilder();
        for (ConstraintViolation cv : ex.getConstraintViolations()) {
            String error = cv.getMessage();
            sb.append(String.format("Atributo %s %s, ", cv.getPropertyPath(), error));
        }
        return ResponseEntity
                .badRequest()
                .body(new ResponseError(HttpStatus.BAD_REQUEST.value(),
                        sb.substring(0, sb.length() - 2),
                        req.getRequestURI()));
    }
}
