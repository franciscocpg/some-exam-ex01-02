/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.shared.exception;

import org.springframework.http.HttpStatus;

/**
 *
 * @author Francisco Guimarães
 * @since 30/06/2016
 */
public class RestException extends Exception {

    private final int code;
    private final String message;

    public RestException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public RestException(HttpStatus httpStatus, String message) {
        this(httpStatus.value(), message);
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
