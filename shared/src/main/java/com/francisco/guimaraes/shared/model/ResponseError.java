/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.francisco.guimaraes.shared.model;

/**
 *
 * @author Francisco Guimarães
 * @since 30/06/2016
 */
public class ResponseError {

    private int code;
    private String message;
    private String path;

    public ResponseError() {
    }

    public ResponseError(int code, String message, String path) {
        this.code = code;
        this.message = message;
        this.path = path;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
