package com.francisco.guimaraes.shared.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Francisco Guimarães
 * @since 26/06/2016
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "rua",
    "numero",
    "complemento",
    "bairro",
    "cep",
    "cidade",
    "estado"
})
@Entity
@Table(name = "ENDERECO")
public class Endereco implements Serializable {

    public interface CEPResponse {
    };

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    @NotNull
    private long id;

    @Size(min = 3, max = 255)
    @Column(name = "RUA", nullable = false)
    @NotNull
    private String rua;

    @Size(max = 10)
    @Column(name = "NUMERO", nullable = false)
    @NotNull
    private String numero;

    @Size(max = 255)
    @Column(name = "COMPLEMENTO")
    private String complemento;

    @Size(max = 255)
    @Column(name = "BAIRRO")
    private String bairro;

    @Pattern(regexp = "[0-9]{5}[0-9]{3}")
    @Size(min = 8, max = 8)
    @Column(name = "CEP", nullable = false)
    @NotNull
    private String cep;

    @Size(max = 255)
    @Column(name = "CIDADE", nullable = false)
    @NotNull
    private String cidade;

    @Size(max = 255)
    @Column(name = "ESTADO", nullable = false)
    @NotNull
    private String estado;

    public Endereco() {
    }

    private Endereco(String rua, String numero, String complemento, String bairro, String cep, String cidade, String estado) {
        this.rua = rua;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cep = cep;
        this.cidade = cidade;
        this.estado = estado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonView(CEPResponse.class)
    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @JsonView(CEPResponse.class)
    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @JsonView(CEPResponse.class)
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @JsonView(CEPResponse.class)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public static class EnderecoBuilder {

        private String rua;
        private String numero;
        private String complemento;
        private String bairro;
        private String cep;
        private String cidade;
        private String estado;

        public EnderecoBuilder() {
        }

        public EnderecoBuilder setRua(String rua) {
            this.rua = rua;
            return this;
        }

        public EnderecoBuilder setNumero(String numero) {
            this.numero = numero;
            return this;
        }

        public EnderecoBuilder setComplemento(String complemento) {
            this.complemento = complemento;
            return this;
        }

        public EnderecoBuilder setBairro(String bairro) {
            this.bairro = bairro;
            return this;
        }

        public EnderecoBuilder setCep(String cep) {
            this.cep = cep;
            return this;
        }

        public EnderecoBuilder setCidade(String cidade) {
            this.cidade = cidade;
            return this;
        }

        public EnderecoBuilder setEstado(String estado) {
            this.estado = estado;
            return this;
        }

        public Endereco createEndereco() {
            return new Endereco(rua, numero, complemento, bairro, cep, cidade, estado);
        }

    }

}
